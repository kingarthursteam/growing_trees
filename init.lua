-------------------------------------------------------------------------------
-- Growing Trees Mod by Sapier
-- 
-- License GPLv3
--
--! @file init.lua
--! @brief main module file responsible for including all parts of growing tees
--! mod
--! @copyright Sapier
--! @author Sapier
--! @date 2012-09-04
--
-- Contact sapier a t gmx net
-------------------------------------------------------------------------------
growing_trees = {}

growing_trees.version = "0.0.9"

local modpath = minetest.get_modpath("growing_trees")

dofile (modpath .. "/type_declarations.lua")
dofile (modpath .. "/models.lua")
dofile (modpath .. "/trunk_functions.lua")
dofile (modpath .. "/branch_functions.lua")
dofile (modpath .. "/generic_functions.lua")
dofile (modpath .. "/nodes.lua")
dofile (modpath .. "/crafts.lua")
dofile (modpath .. "/model_selection.lua")
dofile (modpath .. "/abms.lua")
dofile (modpath .. "/spawning.lua")

MAX_TREE_SIZE = minetest.settings:get("growing_trees_max_size") or 20
SLOWDOWN_TREE_GROWTH_SIZE = minetest.settings:get("growing_trees_slowdown_size") or 10

function growing_trees_debug(loglevel,text)
    --minetest.log(loglevel,text)
    --print(loglevel .. ": " .. text)
end

print("growing_trees mod " .. growing_trees.version .. " loaded")
