-------------------------------------------------------------------------------
-- Growing Trees Mod by Sapier
-- 
-- License GPLv3
--
--! @file type_declarations.lua
--! @brief file containing node to type mappings
--! @copyright Sapier
--! @author Sapier
--! @date 2012-09-04
--
-- Contact sapier a t gmx net
-------------------------------------------------------------------------------

branch_type = {
            ["growing_trees:branch"] = true,
            ["growing_trees:branch_ukn"] = true,
            ["growing_trees:branch_zz"] = true,
            ["growing_trees:branch_xx"] = true,
            ["growing_trees:branch_xpzp"] = true,
            ["growing_trees:branch_xpzm"] = true,
            ["growing_trees:branch_xmzp"] = true,
            ["growing_trees:branch_xmzm"] = true,
            ["growing_trees:branch_sprout"] = true,
            }
            
branch_static_type = {
            ["growing_trees:branch"] = true,
            ["growing_trees:branch_ukn"] = true,
            ["growing_trees:branch_zz"] = true,
            ["growing_trees:branch_xx"] = true,
            ["growing_trees:branch_xpzp"] = true,
            ["growing_trees:branch_xpzm"] = true,
            ["growing_trees:branch_xmzp"] = true,
            ["growing_trees:branch_xmzm"] = true,
}
            
trunk_type = {
            ["growing_trees:trunk_top"] = true,
			["growing_trees:trunk"] = true,
			["growing_trees:medium_trunk"] = true,
			["growing_trees:big_trunk"] = true,
			["growing_trees:trunk_sprout"] = true
}

trunk_static_type = {
			["growing_trees:trunk"] = true,
			["growing_trees:medium_trunk"] = true,
			["growing_trees:big_trunk"] = true
}

leaves_type = {
			["growing_trees:leaves"] = true
}

-------------------------------------------------------------------------------
-- name: growing_trees_node_is_type(table_to_check,name)
--
-- @brief check if a table contains a specific element
--
-- @param table_to_check table to search in
-- @param name name to search
-- @return true/false
-------------------------------------------------------------------------------
function growing_trees_node_is_type(type_declaration,name)
    return type_declaration[name] == true
end

-------------------------------------------------------------------------------
-- name: growing_trees_pos_is_type(table_to_check,pos)
--
-- @brief check if a table contains a specific element
--
-- @param table_to_check table to search in
-- @param pos pos to search
-- @return true/false
-------------------------------------------------------------------------------
function growing_trees_pos_is_type(type_declaration,pos)
	local node = minetest.get_node(pos)
	
    return type_declaration[node.name] == true
end

function growing_trees_get_type_list(type_declaration)
    local list = {}
	for k, v in pairs(type_declaration) do
        table.insert(list,k)
    end
    return list
end
